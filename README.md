# ftp-clean-deploy

 Grunt plugin to clean and deploy files over FTP. You can use the tasks seperately. First run the clean task to empty the FTP root (/), then run the deploy task to publish the files over FTP. This plugin is meant to be used for deploying production code that is a result of a build process producing files. It is not intended to be used to  upload large data.

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

This plugin is not yet published on npmjs.org, but you can still install it with npm using the following command
```shell
npm install git+ssh://git@bitbucket.org/dejan_mladenovski/ftp-clean-deploy.git --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('ftp-clean-deploy');
```

## The "ftp-clean" and  "ftp-deploy" tasks

### Overview
In your project's Gruntfile, add sections named `ftp-clean` and `ftp-deploy` to the data object passed into `grunt.initConfig()`. These tasks are `NOT` codependant and can be used separately.

```js
grunt.initConfig({
  'ftp-deploy': {
    dev: {
        options: {
            src: 'dist',
            dest: 'path/relative/to/ftp/root'
        },
        auth: {
            host: 'ip address or hostname',
            port: 21,
            authKey: 'dev'
        }
    },
    test: {
        options: {
            src: 'dist',
            dest: 'path/relative/to/ftp/root'
        },
        auth: {
            host: 'ip address or hostname',
            port: 21,
            username: 'username', //not recommended!
            password: 'password' //not recommended!
        }
    }
  },
  'ftp-clean': {
    dev: {
        auth: {
            host: '127.0.0.1',
            port: 21,
            authKey: 'dev'
        }
    },
    test: {
        auth: {
            host: 'localhost',
            port: 21,
            username: 'username', //not recommended!
            password: 'password' //not recommended!
        }
    }
  }
});
```

### Options

#### options.src
Type: `String`  
Required: `true`

A string value that determinates the source where the files can be found (relative to Gruntfile.js).

#### options.dest
Type: `String`  
Required: `true`

A string value that determines where on the FTP host should the dest files be copied. If on root this value should be `''`

#### auth.host
Type: `String`  
Required: `true`

A string value that indicates the host address or name. `This is required and does not have a default value`.

#### auth.port
Type: `Integer`  
Required: `true`

An integer value that indicates the port. `This is required and does not have a default value`.

#### auth.username
Type: `String`  
Required: `false`

A string value that defines the username needed to connect to the FTP server. This is not recommended to be kept in the gruntfile. It should be kept in the `.ftpauth` file and this file should not be committed on the repository.

#### auth.password
Type: `String`  
Required: `false`

A string value that defines the password needed to connect to the FTP server. This is not recommended to be kept in the gruntfile. It should be kept in the `.ftpauth` file and this file should not be committed on the repository.

#### auth.authKey
Type: `String`  
Required: `false`

A string value that defines the name of the object that contains the login parameters in the .ftpauth file.
Example:
```js
  {
    "test": {
        "username": "username",
        "password": "password" 
    },
    "dev": {
        "username": "dev",
        "password": "devP" 
    },...
  }
```

### Usage Examples

#### FTP clean
In this example, the task `ftp-clean` will remove all files and folders in the root of the FTP server.

```js
grunt.initConfig({
  'ftp-clean': {
     test: {
        auth: {
            host: 'localhost',
            port: 21,
            username: 'username', //not recommended!
            password: 'password' //not recommended!
        }
    }
  },...
});
```

#### FTP deploy
In this example, the task `ftp-clean` will remove all files and folders in the root of the FTP server.

```js
grunt.initConfig({
  'ftp-deploy': {
     test: {
        options: {
            src: 'dist',
            dest: '/'
        },
        auth: {
            host: 'localhost',
            port: 21,
            username: 'username', //not recommended!
            password: 'password' //not recommended!
        }
    }
  },...
});
```

## Dependancies
This module is depends on `async`, `jsftp` and `lodash` npm modules.

## Contributing
This is a simple module that wraps the [jsftp](https://github.com/sergi/jsftp) library into a grunt task. It was designed to be used for private use.

## Release History
* 2015-09-29 v1.0.0 Initial commit
