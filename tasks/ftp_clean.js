/*
 * ftp-clean-deploy
 *
 * Copyright (c) 2015 Dejan Mladenovski
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  grunt.util = grunt.util || grunt.utils;

  var log = grunt.log;
  var verbose = grunt.verbose;
  var _ = require('lodash');
  var file = grunt.file;
  var fs = require('fs');
  var path = require('path');
  var Ftp = require('jsftp');
  var async = require('async');

  var authVals;
  var forceVerbose;
  var ftp;

  /**
  * Folder object: {path: '/lib/font-awesome', level: 1}
  * Remove the folders by level, from highest to lowest. Root level = 0
  */
  var folders = [];


  function getAuthVals(inAuth) {
    var tmpData;
    var authFile = path.resolve(inAuth.authPath || '.ftpauth');

    // If authentication values are provided in the grunt file itself
    var username = inAuth.username;
    var password = inAuth.password;
    if (typeof username != 'undefined' && username != null && typeof password != 'undefined' && password != null) return {
      username: username,
      password: password
    };

    // If there is a valid auth file provided
    if (fs.existsSync(authFile)) {
      tmpData = JSON.parse(grunt.file.read(authFile));
      if (inAuth.authKey) return tmpData[inAuth.authKey] || {};
      if (inAuth.host) return tmpData[inAuth.host] || {};
    } else if (inAuth.authKey) grunt.warn('\'authKey\' configuration provided but no valid \'.ftpauth\' file found!');

    return {};
  }

  /**
  * parseRoot will get a list of data from the ftp server, and call parse folders method. it will parse all root folders. when finished, it will relete all of them
  */
  function parseRoot(callback){
    ftp.ls('/', function(err, data){
      if(err) {
        log.error('There was a problem parsing the root folder! Error: ' + err);
        callback(err);
      }
      else{
        //get folders and add them to the folders array
        var dirs = _.where(data, {'type': 1});
        dirs.forEach(function(dir){
          folders.push({'dir': path.normalize('/' + path.sep + dir.name).replace(/\\/gi, '/'), 'level': 0});
        });

        //remove files 
        var files = _.where(data, {'type': 0});
        async.eachSeries(files, 
          function(file, callb){
            deleteFile(file.name, callb);
          },
          function(){
            //finished removing files, parse folders in root
            async.eachSeries(dirs,
              function(dir, callb){
                var d = path.normalize('/' + path.sep + dir.name).replace(/\\/gi, '/');
                parseFolder(d, 1, callb);
              },
              function(){
                //done parsing root folders, and all child folders. call callback
                callback(null);
              });
          });
      }
    });
  }
  /**
  * If folder has files remove them
  * if folder
  */
  function parseFolder(location, level, _callback){
    ftp.ls(location, function(err, data){
      if(err){
        log.error('There was a problem parsing the location: ' + location.red + ' ERROR: ' + err);
        _callback(null);
      }
      else{
        //get the folders and add them to the folders array
        var dirs = _.where(data, {'type': 1});
        dirs.forEach(function(dir){
          folders.push({'dir': path.normalize(location + path.sep + dir.name).replace(/\\/gi, '/'), 'level': level});
        });
        //remove files
        var files = _.where(data, {'type': 0});
        async.eachSeries(files,
          function(file, _callb){
            var fPath = path.normalize(location + path.sep + file.name).replace(/\\/gi, '/');
            deleteFile(fPath, _callb);
          },
          function(){
            //done deleting files, process folders
            async.eachSeries(dirs,
              function(dir, _cb){
                var dPath = path.normalize(location + path.sep + dir.name).replace(/\\/gi, '/');
                parseFolder(dPath, level+1, _cb);
              },
              function(){
                //done processing folders, run callback
                _callback(null);
              });
          });
      }
    }); 
  }

  function deleteFolders(_end){
    //order folders by level desc 
    var levels = _.sortByOrder(folders, ['level'], ['desc']);
    
    async.eachSeries(levels, deleteFolder,
      function(){
        //done removing folders
        _end(null);
      });
  }

  function deleteFile(path, cb){
    ftp.raw.dele(path, function(err){
      if(err) log.error('Unable to remove file: ' + path.red + '   ERROR: ' + err);
      else log.ok('File ' + path + ' was removed!');
      cb(null);
    });
  }

  function deleteFolder(folder, end){
    ftp.raw.rmd(folder.dir, function(err){
      if(err){
        log.error('Unable to delete the folder: ' + folder.dir.red + ' ERROR: ' + err);
      }
      else{
        log.ok('Folder ' + folder.dir.green + ' was deleted.');
      }
      end(null);
    });
  }

  grunt.registerMultiTask('ftp-clean', 'Clean root folder in FTP.', function() {
    var done = this.async();

    // Init ftp object
    ftp = new Ftp({
      host: this.data.auth.host,
      port: this.data.auth.port,
      onError: done
    });

    authVals = getAuthVals(this.data.auth);
    forceVerbose = this.data.forceVerbose === true ? true : false;

    //check if auth values exist
    if(!authVals.username || !authVals.password) grunt.warn('No ftp credentials provided!');

    //connect to ftp and upload files
    ftp.auth(authVals.username, authVals.password, function(err){
      if(err) grunt.warn('FTP Auth failed ' + err);

      //successfully connected, read all data from ftp
      parseRoot(function(err){
        if(err){ log.error('There was an error parsing the folders and subfolders!');}
        else{
          //no errors, delete folders
          deleteFolders(function(err){
            if(err){
              log.error('There was an error deleting the folders!');
            }
            else{
              log.ok('Root is empty!');
            }
            //close ftp connection and exit
            ftp.raw.quit(function(err){
                if(err) log.error(err);
                done();
            });
          });
        }
      });
    });
    if(grunt.errors) return false;
  });

};
