/*
 * ftp-clean-deploy
 *
 * Copyright (c) 2015 Dejan Mladenovski
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  grunt.util = grunt.util || grunt.utils;

  var log = grunt.log;
  var verbose = grunt.verbose;
  var _ = require('lodash');
  var file = grunt.file;
  var fs = require('fs');
  var path = require('path');
  var Ftp = require('jsftp');
  var async = require('async');

  var localRoot;
  var remoteRoot;
  var authVals;
  var toTransfer;
  var forceVerbose;
  var ftp;
  var currPath;
  var totalFiles = 0;
  var totalUploadedFiles = 0;

  // A method for parsing the source location and storing the information into a suitably formated object
  function dirParseSync (startDir, result) {
    var files;
    var i;
    var tmpPath;
    var currFile;

    // initialize the `result` object if it is the first iteration
    if (result === undefined) {
      result = {};
      result[path.sep] = [];
    }

    // check if `startDir` is a valid location
    if (!fs.existsSync(startDir)) {
      grunt.warn(startDir + ' is not an existing location');
    }

    // iterate throught the contents of the `startDir` location of the current iteration
    files = fs.readdirSync(startDir);
    for (i = 0; i < files.length; i++) {
      currFile = startDir + path.sep + files[i];
      if (file.isDir(currFile)) {
        tmpPath = path.relative(localRoot, startDir + path.sep + files[i]);
        if (!_.has(result, tmpPath)) {
          result[tmpPath] = [];
        } 
        dirParseSync(startDir + path.sep + files[i], result);
      } else {
        totalFiles++;
        tmpPath = path.relative(localRoot, startDir);
        if (!tmpPath.length) {
          tmpPath = path.sep;
        }
        result[tmpPath].push(files[i]);
      }
    }
    return result;
  }

  function getAuthVals(inAuth) {
    var tmpData;
    var authFile = path.resolve(inAuth.authPath || '.ftpauth');

    // If authentication values are provided in the grunt file itself
    var username = inAuth.username;
    var password = inAuth.password;
    if (typeof username != 'undefined' && username != null && typeof password != 'undefined' && password != null) return {
      username: username,
      password: password
    };

    // If there is a valid auth file provided
    if (fs.existsSync(authFile)) {
      tmpData = JSON.parse(grunt.file.read(authFile));
      if (inAuth.authKey) return tmpData[inAuth.authKey] || {};
      if (inAuth.host) return tmpData[inAuth.host] || {};
    } else if (inAuth.authKey) grunt.warn('\'authKey\' configuration provided but no valid \'.ftpauth\' file found!');

    return {};
  }


  function createLocation(location, cb){
    if(location === '/'){
      log.ok('Skipped creating root folder!');
      cb(null);
    }
    else{
      ftp.raw.mkd(location, function (err) {
        if(err) {
          log.error('Error creating new remote folder ' + location + ' --> ' + err);
          cb(null);
        } else {
          log.ok('New remote folder created ' + location.yellow);
          cb(null);
        }
      });
    }
  }

  function uploadFile(localPath, remotePath, tries, callback) {
    if(tries < 3){
      ftp.put(localPath, remotePath, function(err){
        tries++;
        if(err) { 
          log.error('Failed uploading file: ' + localPath.red + ' -- tries: ' + tries); 
          //try again
          uploadFile(localPath, remotePath, tries, callback);
        }
        else { totalUploadedFiles++; log.ok('Upload complete: ' + localPath.green); callback(null);}
      });
    }
    else{
      log.error('Unable to upload file: ' + localPath);
      callback(err);
    }
  }

  grunt.registerMultiTask('ftp-deploy', 'Create folder structure and upload files over FTP.', function() {
    var done = this.async();

    // Init ftp object
    ftp = new Ftp({
      host: this.data.auth.host,
      port: this.data.auth.port,
      onError: done
    });

    localRoot = this.data.options.src;
    remoteRoot = this.data.options.dest;
    authVals = getAuthVals(this.data.auth);
    toTransfer = dirParseSync(localRoot);
    forceVerbose = this.data.forceVerbose === true ? true : false;

    //check if auth values exist
    if(!authVals.username || !authVals.password) grunt.warn('No ftp credentials provided!');

    //connect to ftp and upload files
    ftp.auth(authVals.username, authVals.password, function(err){
      if(err) grunt.warn('FTP Auth failed ' + err);
      
      var locations = _.keys(toTransfer);

      //iterate through all locations and create the folders. When done upload the files
      async.eachSeries(locations, 
        function(location, callb) {
          var inPath = path.normalize('/' + remoteRoot + '/' + location).replace(/\\/gi, '/');
          createLocation(inPath, callb);
      }, 
        function() {
          //if done creating folders, upload files. for each path get the files and upload them
          async.eachSeries(locations,
            function(location, callb) {
              var files = toTransfer[location];
              if(files.length > 0){
                async.eachSeries(files, 
                  function(file, callback){
                    var localFile = path.normalize(localRoot + path.sep + location + path.sep + file).replace(/\\/gi, '/');
                    var remoteFile = path.normalize(remoteRoot + path.sep + location + path.sep + file).replace(/\\/gi, '/');
                    uploadFile(localFile, remoteFile, 0, callback);
                  },
                  function(){
                    log.ok('Upload complete for -> ' + location.yellow);
                    callb(null);
                  });
              }
              else{
                log.ok('No files to upload in -> ' + location.yellow);
                callb(null);
              }
            },
            function(){
              //upload complete, close ftp connection
              ftp.raw.quit(function(err){
                if(err) log.error(err);
                else log.ok('FTP upload completed!');
                log.ok('TOTAL FILES: ' + totalFiles + ' TOTAL UPLOADED: ' + totalUploadedFiles);
                if(totalUploadedFiles === totalFiles){
                  var msg = 'All files uploaded successfully! Total uploaded files: ' + totalUploadedFiles;
                  log.ok(msg.green);
                }
                else{
                 var msg = 'Problem uploading all files! Total uploaded files: ' + totalUploadedFiles;
                  log.ok(msg.yellow); 
                }
                done();
              });
            });
        
      });
    });
    if(grunt.errors) return false;
  });

};
